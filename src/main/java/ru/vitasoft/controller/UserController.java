package ru.vitasoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vitasoft.entity.User;
import ru.vitasoft.entity.UserRole;
import ru.vitasoft.repository.RoleRepository;
import ru.vitasoft.servise.RoleService;
import ru.vitasoft.servise.TaskService;
import ru.vitasoft.servise.UserService;

import java.util.List;
import java.util.Set;

import static org.springframework.http.HttpStatus.*;


@RestController
public class UserController {


    @Autowired
    public TaskService taskService;

    @Autowired
    public UserService userService;


    @Autowired
    public RoleService roleService;


    private User authUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return  userService.getUserByUsername(auth.getName());
    }

    @GetMapping("/user/get/users")
    public ResponseEntity getUsers(){
        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_ADMIN"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping("/user/set/role/operator")
    public ResponseEntity setRoleOperator(Long id){
        if (id != null && authUser().getUserRoles().contains(roleService.getRole("ROLE_ADMIN")) && userService.isUser(id)){
            User user = userService.getUsersById(id);
            UserRole operatorRole =  roleService.getRole("ROLE_OPERATOR");
            userService.addRole(user, operatorRole);
            return ResponseEntity.status(OK).build();
        }
        return ResponseEntity.status(BAD_REQUEST).build();
    }


}
