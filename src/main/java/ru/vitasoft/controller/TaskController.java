package ru.vitasoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vitasoft.entity.Task;
import ru.vitasoft.entity.TaskState;
import ru.vitasoft.entity.User;
import ru.vitasoft.entity.UserRole;
import ru.vitasoft.entity.parameters.TaskRequestParameters;
import ru.vitasoft.repository.RoleRepository;
import ru.vitasoft.repository.UserRepository;
import ru.vitasoft.servise.RoleService;
import ru.vitasoft.servise.TaskService;
import ru.vitasoft.servise.UserService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.HttpStatus.*;

@RestController
public class TaskController {

    @Autowired
    public TaskService taskService;

    @Autowired
    public UserService userService;


    @Autowired
    public RoleService roleService;


    private User authUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService.getUserByUsername(auth.getName());
    }

    @GetMapping("/task/create")
    public ResponseEntity createTask(TaskRequestParameters requestParameters) {

        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_USER"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }

        Calendar calendar = new GregorianCalendar();
        Task task = new Task(requestParameters.getTaskState()
                , requestParameters.getMsg()
                , calendar.getTime()
                , userService.getUsersById(authUser().getId()));
        taskService.saveTask(task);
        return ResponseEntity.status(OK).build();
    }

    @GetMapping("/task/approve")
    public ResponseEntity approveTask(Long id) {

        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_OPERATOR"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }

        if (id != null) {
            Task task = taskService.getTaskById(id);
            if (task != null) {
                task.setTaskState(TaskState.APPROVED);
                taskService.saveTask(task);
                return ResponseEntity.status(OK).build();
            }
        }
        return ResponseEntity.status(BAD_REQUEST).build();
    }


    @GetMapping("/task/reject")
    public ResponseEntity rejectTask(Long id) {

        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_OPERATOR"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }

        if (id != null) {
            Task task = taskService.getTaskById(id);
            if (task != null) {
                task.setTaskState(TaskState.REJECTED);
                taskService.saveTask(task);
                return ResponseEntity.status(OK).build();
            }
        }
        return ResponseEntity.status(BAD_REQUEST).build();
    }


    @GetMapping("/task/set/send")
    public ResponseEntity setTaskStatusSend(Long id) {

        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_USER"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }

        if (id != null) {
            Task task = taskService.getTaskById(id);
            if (task != null && task.getAuthor().equals(authUser()) && task.getTaskState().equals(TaskState.DRAFT)) {
                task.setTaskState(TaskState.SEND);
                taskService.saveTask(task);
                return ResponseEntity.status(OK).build();
            }
        }
        return ResponseEntity.status(BAD_REQUEST).build();
    }

    @GetMapping("/task/edit")
    public ResponseEntity setTask(TaskRequestParameters taskRequestParameters) {

        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_USER"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }

        if (taskRequestParameters.getId() != null) {
            Task task = taskService.getTaskById(taskRequestParameters.getId());
            if (task != null && task.getAuthor().equals(authUser()) && task.getTaskState().equals(TaskState.DRAFT)) {
                task.setTaskState(taskRequestParameters.getTaskState());
                task.setMsg(taskRequestParameters.getMsg());
                taskService.saveTask(task);
                return ResponseEntity.status(OK).build();
            }
        }
        return ResponseEntity.status(BAD_REQUEST).build();
    }

    @GetMapping("/task/get/all_sent_tasks")
    public ResponseEntity<List<Task>> userSentTasksList() {
        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_OPERATOR"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }
        return ResponseEntity.ok(taskService.getSendTasks());
    }

    @GetMapping("/task/get/all_my_tasks")
    public ResponseEntity<List<Task>> userTasksList() {
        if (!authUser().getUserRoles().contains(roleService.getRole("ROLE_USER"))) {
            return ResponseEntity.status(FORBIDDEN).build();
        }
        return ResponseEntity.ok(taskService.getTasksByAuthor(authUser()));
    }

    @GetMapping("/test/get/task")
    public ResponseEntity<Task> getTask(Long id) {
        Task task = null;
        if (id != null && taskService.isTask(id)) {
            task = taskService.getTaskById(id);
        } else ResponseEntity.notFound();
        if (task.getAuthor().getId() == authUser().getId() && authUser().getUserRoles().contains(roleService.getRole("ROLE_USER")))
        {
            return ResponseEntity.ok(task);
        }

        if (task.getTaskState().equals(TaskState.SEND) && authUser().getUserRoles().contains(roleService.getRole("ROLE_OPERATOR")))
        {
            StringBuffer s1= new StringBuffer();
            for (char chars : task.getMsg().toCharArray()) {
                s1.append(chars).append("-");
            }
            s1.deleteCharAt(s1.length()-1);
            task.setMsg(s1.toString());
            return ResponseEntity.ok(task);
        }
        return ResponseEntity.status(FORBIDDEN).build();
    }

  /*  @Autowired
    @GetMapping("/task/test")
    public ResponseEntity testTasks(RoleRepository roleRepository, UserRepository userRepository) {

        UserRole userRole = new UserRole(1, "ROLE_USER");
        UserRole operatorRole = new UserRole(2, "ROLE_OPERATOR");
        UserRole adminRole = new UserRole(3, "ROLE_ADMIN");

        roleRepository.save(userRole);
        roleRepository.save(operatorRole);
        roleRepository.save(adminRole);

        Set<UserRole> adminSetRole = new HashSet<>();
        adminSetRole.add(adminRole);
        Set<UserRole> operatorSetRole = new HashSet<>();
        operatorSetRole.add(operatorRole);
        Set<UserRole> userSetRole = new HashSet<>();
        userSetRole.add(userRole);



        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(8);
        String hashedAdmin = passwordEncoder.encode("admin");
        String hashedPassword = passwordEncoder.encode("password");
        System.out.println("hashedAdmin " + hashedAdmin);
        System.out.println("hashedPassword " + hashedPassword);

        User admin = new User( "admin", "password", "admin", "admin", adminSetRole);

        User operator = new User( "operator", "operator", "ROLE_OPERATOR", "2", operatorSetRole);

        User user1 = new User( "user1", "user", "ROLE_USER", "3", userSetRole);
        User user4 = new User( "user4", "user", "ROLE_USER", "4", userSetRole);


        userService.saveUser(user1);
        userService.saveUser(operator);
        userService.saveUser(admin);
        userService.saveUser(user4);

        return ResponseEntity.status(OK).build();
    }*/
}
