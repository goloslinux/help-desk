package ru.vitasoft.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vitasoft.entity.UserRole;
import ru.vitasoft.repository.RoleRepository;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public UserRole getRole(String nameRole) {

        return roleRepository.findByName(nameRole);
    }
}
