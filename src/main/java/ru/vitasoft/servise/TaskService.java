package ru.vitasoft.servise;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import ru.vitasoft.entity.Task;
import ru.vitasoft.entity.TaskState;

import ru.vitasoft.entity.User;
import ru.vitasoft.repository.TaskRepository;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;


    public void saveTask(Task task) {
        taskRepository.save(task);
    }

//    public List<Task> getTasksByAuthor(User user) {
//        List<Task> tasksFromDB = taskRepository.findByAuthor(user);
//        return tasksFromDB;
//    }

    public boolean isTask(long id){
        return taskRepository.existsById(id);
    }

    public Task getTaskById(long id) {
        Optional<Task> tasksFromDB = taskRepository.findById(id);
        return tasksFromDB.orElse(null);
    }

    public List<Task> getSendTasks(){
//        Sort.by(Sort.Direction.ASC, "date")
        List<Task> tasksFromDB = taskRepository.findByTaskStateOrderByDateAsc(TaskState.SEND);
        return tasksFromDB;
    }

    public boolean sendTask(Task task) {
        Optional<Task> taskFromDB = taskRepository.findById(task.getId());
 // Может быть тут лучше проверить через taskRepository.exists() taskRepository.existsById()
        if (taskFromDB.isPresent()) {
            task.setTaskState(TaskState.SEND);
            taskRepository.save(task);
        } else return false;

        return true;
    }

    public boolean approvedTask(Task task) {
        Optional<Task> taskFromDB = taskRepository.findById(task.getId());

        // Может быть тут лучше проверить через taskRepository.exists() taskRepository.existsById()
        if (taskFromDB.isPresent()) {
            task.setTaskState(TaskState.APPROVED);
            taskRepository.save(task);
        } else return false;

        return true;
    }

    public boolean rejectTask(Task task) {
        Optional<Task> taskFromDB = taskRepository.findById(task.getId());

// Может быть тут лучше проверить через taskRepository.exists() taskRepository.existsById()
        if (taskFromDB.isPresent()) {
            task.setTaskState(TaskState.REJECTED);
            taskRepository.save(task);
        } else return false;

        return true;
    }

    public List<Task> getSendTasksByAuthor(User author) {
        return taskRepository.findByAuthor(author);
    }

    public List<Task> getTasksByAuthor(User author) {
        return taskRepository.findByAuthor(author);
    }
}
