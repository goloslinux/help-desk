package ru.vitasoft.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.vitasoft.entity.User;
import ru.vitasoft.entity.UserRole;
import ru.vitasoft.repository.RoleRepository;
import ru.vitasoft.repository.UserRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public void addRole(User user, UserRole role) {
        user.getUserRoles().add(role);
        userRepository.save(user);
    }

    public boolean isUser(long id){
        return userRepository.existsById(id);
    }

    public User getUsersById(long authorId) {
        return userRepository.getOne(authorId);
    }

    public User getUserByUsername(String username){
        User user = userRepository.findByUsername(username);

        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }

//        List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority('user'));

        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }

//        List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority('user'));

        return user;

    }

    public boolean  saveUser(User user){

        User userFromDB = userRepository.findByUsername(user.getUsername());


        user.setPassword(
                bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }
}
