package ru.vitasoft.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.vitasoft.servise.UserService;

@Configuration
@EnableConfigurationProperties
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    UserService userService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {




        http
                .httpBasic()
                .and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/user/get/users").hasRole("ADMIN")
                .antMatchers("/task/").hasAnyRole("USER", "OPERATOR")
                .antMatchers("/task/create").hasRole("USER")
                .antMatchers("/task/approve").hasRole("OPERATOR")
                .antMatchers("/task/reject").hasRole("OPERATOR")
                .antMatchers("/task/set/send").hasRole("USER")
                .antMatchers("/task/get/all_sent_tasks").hasRole("OPERATOR")
                .antMatchers("/task/get/my_all_sent_tasks").hasRole("USER")
                .antMatchers("/test/get/task").hasAnyRole("USER", "OPERATOR")
                .antMatchers("/task/edit").hasRole("USER")
                .anyRequest()
                .authenticated()
                .and()
                .headers().frameOptions().disable()

        ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService)
                .passwordEncoder(bCryptPasswordEncoder());
    }
}
