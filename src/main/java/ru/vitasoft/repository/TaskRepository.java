package ru.vitasoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vitasoft.entity.Task;
import ru.vitasoft.entity.TaskState;
import ru.vitasoft.entity.User;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task,Long> {
//    Task findById(String id);

    List<Task> findByAuthor(User user);
    List<Task> findByTaskStateOrderByDateAsc(TaskState taskState);
}
