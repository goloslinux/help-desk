package ru.vitasoft.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.vitasoft.entity.UserRole;

public interface RoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findByName(String role);
}
