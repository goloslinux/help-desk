package ru.vitasoft.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vitasoft.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername (String username);
}
