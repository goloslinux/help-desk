package ru.vitasoft.entity.parameters;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.vitasoft.entity.TaskState;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskRequestParameters {

    private Long id;
    private TaskState taskState;

    private String msg;

//    private Date date;
//    private long authorId;

}
