package ru.vitasoft.entity;

import java.util.stream.Stream;

public enum TaskState {
    DRAFT(100), SEND(200), APPROVED(300), REJECTED(400);

    private int taskState;

    private TaskState(int taskState) {
        this.taskState = taskState;
    }

    public int getTaskState() {
        return taskState;
    }

    public static TaskState of(int taskState) {
        return Stream.of(TaskState.values())
                .filter(t -> t.getTaskState() == taskState)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
