package ru.vitasoft.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    private TaskState taskState;

    private String msg;

    private Date date;

    @ManyToOne(fetch = FetchType.EAGER)
    private User author;

    public Task(TaskState taskState, String msg, Date date, User author) {
        this.taskState = taskState;
        this.msg = msg;
        this.date = date;
        this.author = author;
    }
}
