package ru.vitasoft.servise;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vitasoft.entity.Task;
import ru.vitasoft.entity.TaskState;
import ru.vitasoft.entity.User;
import ru.vitasoft.entity.UserRole;
import ru.vitasoft.repository.RoleRepository;
import ru.vitasoft.repository.TaskRepository;
import ru.vitasoft.repository.UserRepository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskService taskService;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    RoleService roleService;

    @Autowired
    RoleRepository roleRepository;

    @Test
    void getUsers() {

        UserRole userRole = new UserRole(1, "USER");
        UserRole operatorRole = new UserRole(2, "OPERATOR");
        UserRole adminRole = new UserRole(3, "ADMIN");

        roleRepository.save(userRole);
        roleRepository.save(operatorRole);
        roleRepository.save(adminRole);

        Set<UserRole> adminSetRole=new HashSet<>();
        adminSetRole.add(adminRole);
        User admin= new User(1, "admin", "password", "damin", "1", adminSetRole);

        Set<UserRole> operatorSetRole=new HashSet<>();
        operatorSetRole.add(operatorRole);
        User operator= new User(2, "operator", "operator", "operator", "2", operatorSetRole);

        Set<UserRole> userSetRole=new HashSet<>();
        userSetRole.add(userRole);
        User user= new User(3, "user", "user", "user", "3", userSetRole);


        userRepository.save(user);
        userRepository.save(operator);
        userRepository.save(admin);
//
//
//
//        Calendar calendar = new GregorianCalendar();
//
//        Task adminTask=new Task(TaskState.APPROVED, "Msg", calendar.getTime(), admin);
//        Task operatorTask=new Task(TaskState.APPROVED, "Msg", calendar.getTime(), admin);
//        Task userTask=new Task(TaskState.APPROVED, "Msg", calendar.getTime(), admin);
//
//        taskService.saveTask(adminTask);
//        taskService.saveTask(operatorTask);
//        taskService.saveTask(userTask);
//
//        userService.getUsers();
//        System.out.println(userService.getUsers());
    }

    @Test
    void addRole() {
        String s = "fasdfdas fasdasdfas asdff asdf";
        StringBuffer s1= new StringBuffer();
        for (char chars : s.toCharArray()) {
            s1.append(chars).append("-");
        }
        s1.deleteCharAt(s1.length()-1);
        System.out.println(s1);
    }

    @Test
    void isUser() {
    }

    @Test
    void getUsersById() {
    }
}